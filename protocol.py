#!/usr/bin/env python
import base64
import csv
import datetime
import json
import os
import time
import traceback

import redis as r
from dotenv import load_dotenv

from utils.pitre import PiTre, PiTreException
from utils.sdc import SDC, INCOME_TYPE, OUTCOME_TYPE
from utils.logger import logger

load_dotenv()

# Environment variables
ENV = os.environ.get("ENV")
ERROR_SLEEP_SECONDS = int(os.environ.get("ERROR_SLEEP_SECONDS", 60))
SUCCESS_SLEEP_SECONDS = int(os.environ.get("SUCCESS_SLEEP_SECONDS", 10))

CONNECTION_TIMEOUT = int(os.environ.get("CONNECTION_TIMEOUT", 10))
READ_TIMEOUT = int(os.environ.get("READ_TIMEOUT", 300))

# Redis initialization
REDIS_URL = os.environ.get("REDIS_URL", None) or None
redis = r.Redis().from_url(REDIS_URL) if REDIS_URL else r.Redis()
REDIS_KEY_EXPIRATION = int(os.environ.get("REDIS_KEY_EXPIRATION", -1))

# Gotenberg
GOTENBERG_URL = os.environ.get("GOTENBERG_URL", None)
SAVE_PDFS = True if os.environ.get("SAVE_PDFS", "false").lower() == 'true' else False
SAVE_XMLS = True if os.environ.get("SAVE_XMLS", "false").lower() == 'true' else False

# SDC filters
API_START_OFFSET = int(os.environ.get("API_START_OFFSET", 0))
API_LIMIT = int(os.environ.get("API_LIMIT", 10))

API_ORDERING = os.environ.get("API_ORDERING", None)
API_SORTING = os.environ.get("API_SORTING", None)
API_VERSION = os.environ.get("API_VERSION", 1)

SERVICES_WHITELIST = os.environ.get("SERVICES_WHITELIST", None)
SERVICES_WHITELIST = SERVICES_WHITELIST.split("|") if SERVICES_WHITELIST else None

STATUSES_WHITELIST = os.environ.get("STATUSES_WHITELIST", None)
STATUSES_WHITELIST = STATUSES_WHITELIST.split("|") if STATUSES_WHITELIST else None

IDS = os.environ.get("IDS", None)
IDS = IDS.split("|") if IDS else None

# Protocol
LIMIT = int(os.environ.get("LIMIT", -1))

## Correnspondeces
SEARCH_CORRESPONDENT = True if os.environ.get("SEARCH_CORRESPONDENT", "false").lower() == 'true' else False
CREATE_CORRESPONDENT = True if os.environ.get("CREATE_CORRESPONDENT", "false").lower() == 'true' else False
CORRESPONDENT_CHANNEL = os.environ.get("CORRESPONDENT_CHANNEL", "MAIL").upper()

## Search parameters
SEARCH_DOC_BY_YEAR = os.environ.get("SEARCH_DOC_BY_YEAR", None)
SEARCH_PRJ_BY_YEAR = os.environ.get("SEARCH_PRJ_BY_YEAR", None)

## Typed project/documents
TYPED_PROJECT = True if os.environ.get("TYPED_PROJECT", "false").lower() == 'true' else False
TYPED_DOCUMENT = True if os.environ.get("TYPED_DOCUMENT", "false").lower() == 'true' else False
TRANSMIT_DOCUMENT = True if os.environ.get("TRANSMIT_DOCUMENT", "false").lower() == 'true' else False
SEND_OUTCOME_DOCUMENT = True if os.environ.get("SEND_OUTCOME_DOCUMENT", "false").lower() == 'true' else False

## Messages
RECORD_RESPONSES = True if os.environ.get("RECORD_RESPONSES", "true").lower() == 'true' else False
RECORD_NOTES = os.environ.get("RECORD_NOTES", "never").lower()
RECORD_MESSAGES = os.environ.get("RECORD_MESSAGES", "never").lower()

RECORD_STATUSES = os.environ.get("RECORD_STATUSES", None)
RECORD_STATUSES = RECORD_STATUSES.split("|") if RECORD_STATUSES else None

## SDC update
UPDATE_SOURCE = True if os.environ.get("UPDATE_SOURCE", "false").lower() == 'true' else False
LOG_LEVEL = os.environ.get("LOG_LEVEL", "INFO")

# OUTPUT
output_file = open("output.csv", mode='a')
output_writer = csv.DictWriter(
    output_file,
    fieldnames=[
        'Application ID',
        'Project',
        'Project Description',
        'Document ID',
        'Document Description',
        'Protocol Number',
        'Signature',
        'Error'
    ]
)

# SDC configuration
config = json.load(open("./config/config.json"))[ENV]
sdc_api_url = config['sdc']['api_url']
sdc_username = config['sdc']['username']
sdc_password = config['sdc']['password']


def redis_mset(key, value):
    """
    Add redis key-value pair
    :param key: redis key
    :param value: redis value
    :return None
    """
    redis.mset({key: value})
    if REDIS_KEY_EXPIRATION > 0:
        redis.expire(key, REDIS_KEY_EXPIRATION)


def log_to_file(writer, application_id, project, project_description, document_id, document_description,
                protocol_number, signature, error):
    """
    Writes a line of the output csv file
    :param writer: writer object
    :param application_id: Id pf the SDC application
    :param project: Pitre project code
    :param project_description: Pitre project object
    :param document_id: Pitre document id
    :param document_description: Pitre document object
    :param protocol_number: Pitre protocol number assigned to application's document
    :param signature: Pitre protocol signature assigned to application's document
    :param error: Error description if an error occurred during the protocol flow
    :return: None
    """
    writer.writerow({
        'Application ID': application_id,
        'Project': project,
        'Project Description': project_description,
        'Document ID': document_id,
        'Document Description': document_description,
        'Protocol Number': protocol_number,
        'Signature': signature,
        'Error': error,
    })


def search_or_create_document(document, correspondent):
    """
    Searches a document inside pitre or creates a new one if not found. If typed documents are enabled template filter
    is applied and document is created according to the template.
    :param document: Document opbject
    :param correspondent: PiTre correspondence
    :return: PiTre document
    """
    document_object = document["document_object"]
    document_type = document["type"]
    arrival_date = document["arrival_date"]
    template_filter = get_typed_document_search_filters(document) if TYPED_DOCUMENT else None
    template = template_filter["Template"] if TYPED_DOCUMENT else None

    if TYPED_DOCUMENT:
        # Typed document is enabled, use template filters
        status, response = pi_tre.search_document(
            document_description=document_object,
            template=template_filter,
            in_protocol=document_type == INCOME_TYPE,
            year=SEARCH_DOC_BY_YEAR
        )
    else:
        # Standard document search
        status, response = pi_tre.search_document(
            document_description=document_object,
            in_protocol=document_type == INCOME_TYPE,
            year=SEARCH_DOC_BY_YEAR
        )

    if status == 1:
        raise PiTreException(response)
    if len(response) == 0:
        logger.info("Document \"{}\" not found.".format(document_object))

        logger.info("Create document \"{}\"".format(document_object))
        # Create PiTre document
        status, response = pi_tre.create_document(
            object=document_object,
            document_type="A" if document_type == INCOME_TYPE else "P",
            template=template,
            sender=correspondent if document_type == INCOME_TYPE else pi_tre.get_default_sender(),
            recipient=pi_tre.get_default_sender() if document_type == INCOME_TYPE else correspondent,
            arrival_date=datetime.datetime.strptime(arrival_date, '%Y-%m-%dT%H:%M:%S%z'),
            note=False if document_type == INCOME_TYPE else True
        )
        if status == 1:
            raise PiTreException(response)
        return response
    else:
        return response[0]


def search_or_create_project(document):
    """
    Searches a project inside pitre or creates a new one if not found. If typed projects are enabled template filter
    is applied and project is created according to the template.
    If a new project had to be created, the transmission will take place
    :param document: Document object
    :return: PiTre project
    """
    project_object = document["project_object"]
    template_filter = get_typed_project_search_filters(document) if TYPED_PROJECT else None
    template = template_filter["Template"] if TYPED_PROJECT else None

    if TYPED_PROJECT:
        # Typed project is enables, use template filters
        status, response = pi_tre.search_projects(
            project_description=project_object,
            template=template_filter,
            year=SEARCH_PRJ_BY_YEAR
        )
    else:
        # Standard project search
        status, response = pi_tre.search_projects(
            project_description=project_object,
            year=SEARCH_PRJ_BY_YEAR
        )

    if status == 1:
        raise PiTreException(response)
    if len(response) == 0:
        logger.info("Project \"{}\" not found.".format(project_object))

        logger.info("Create project \"{}\"".format(project_object))
        # Create Pitre Project
        status, response = pi_tre.create_project(description=project_object, template=template)
        if status == 1:
            raise PiTreException(response)

        project = response
        # Transmission of newly created project
        logger.info("Transmitting project \"{}\"".format(project_object))
        transmit_project(project_id=project["Id"])
        return project
    else:
        return response[0]


def search_or_create_correspondent(document):
    """
    Search for a correspondent in the PiTre address book: the search is done using the fiscal code
    (national identification number).
    If the creation of the correspondent is not enabled, an occasional user will be returned in case of search failure
    :param document: Document object
    :return: PiTre correspondent or occasional correspondent
    """
    # Search PiTre correspondent
    status, response = pi_tre.search_correspondence(
        national_identification_number=document["correspondent"]["fiscal_code"],
        type="EXTERNAL",
        offices="FALSE",
        users="TRUE",
        address_book_rf=True
    )
    if status == 1:
        raise PiTreException(response)
    if len(response) == 0:
        logger.info("Correspondent \"{}\" not found.".format(document["correspondent"]["fiscal_code"]))
        if not CREATE_CORRESPONDENT:
            # Create correspondent is not enabled => create an occasional user
            pitre_correspondent = pi_tre.create_occasional_correspondent(
                description=document["correspondent"]["description"]
            )
            return pitre_correspondent
        else:
            correspondent = document["correspondent"]
            # Create a new correspondent inside the PiTre address book
            status, response = pi_tre.add_correspondent(
                description=correspondent["description"],
                name=correspondent["name"],
                surname=correspondent["surname"],
                national_identification_number=correspondent["fiscal_code"],
                code=correspondent["code"],
                phone_number=correspondent["phone_number"],
                phone_number2=correspondent["phone_number2"],
                email=correspondent["email"],
                other_emails=document["correspondent"]["other_emails"],
                address=correspondent["address"],
                cap=correspondent["cap"],
                city=correspondent["city"],
                province=correspondent["province"],
                location=correspondent["location"],
                nation=correspondent["nation"],
                fax=correspondent["fax"],
                vat_number=correspondent["vat_number"],
                note=correspondent["note"],
                preferred_channel=CORRESPONDENT_CHANNEL
            )
            if status == 1:
                raise PiTreException(response)
            return response
    else:
        return response[0]


def get_updated_document(document_id):
    """
    Search for a document in PiTre given the id
    :param document_id: Pitre document id
    :return: PiTre document
    """
    status, response = pi_tre.get_document(document_id)
    if status == 1:
        raise PiTreException(response)
    return response


def upload_file(file, document_id, attachment=True):
    """
    Upload an attachment to the PiTre document.
    If the url of the file to be uploaded is not present, a new pdf file will be generated according to a
    predefined template
    :param file: File object to upload
    :param document_id: Id of the PiTre document to attach the file to
    :param attachment: Boolean required for differentiating attachments and main documents
    :return: (string) Pitre upload response
    """
    logger.info("Upload attachment \"{}\"".format(file["name"]))
    if "url" in file:
        # get base64 file content from remote url
        file["content"] = base64.b64encode(SDC.get_from_url(file["url"], True)["content"])
    elif "message_data" in file:
        # Generate pdf file using predefined template and get base64 content
        file["content"] = base64.b64encode(SDC.generate_content_from_template(file["message_data"]))
    elif "authentication_data" in file:
        # Generate pdf file using predefined template and get base64 content
        file["content"] = base64.b64encode(SDC.generate_xml(file["authentication_data"]))

    status, response = pi_tre.upload_file_to_document(
        document_id=document_id,
        base64_content=file["content"],
        original_name=file["original_name"],
        description=file["name"],
        create_attachment=attachment
    )
    if status == 1:
        raise PiTreException(response)
    return response


def protocol_document(document_id, project_code):
    """
    Registration of the document
    :param document_id: Id of the PiTre document to be registered
    :param project_code: Code of the PiTre project in which to add the document just registered
    :return:
    """
    # Protocol
    status, response = pi_tre.protocol_predisposed(
        document_id=document_id,
        code_register=project_code
    )
    if status == 1:
        raise PiTreException(response)
    protocolized_document = response
    # Add document in project
    status, response = pi_tre.add_doc_in_project(
        document_id=document_id,
        code_project=project_code
    )
    if status == 1:
        raise PiTreException(response)
    return protocolized_document, response


def transmit_project(project_id):
    # Transmission of newly created project
    for project_model in pi_tre.transmission_project_models:
        logger.info("Execute project trasmission with model \"{}\"".format(project_model))
        status, response = pi_tre.execute_transm_prj_model(id_project=project_id, id_model=project_model)
        if status == 1:
            raise PiTreException(response)


def transmit_document(document_id):
    # Transmission of newly registered document
    for document_model in pi_tre.transmission_document_models:
        logger.info("Execute document trasmission with model \"{}\"".format(document_model))
        status, response = pi_tre.execute_transm_doc_model(id_document=document_id, id_model=document_model)
        if status == 1:
            raise PiTreException(response)
        logger.info(response)


def send_document(document_id):
    # Sending of newly registered document
    status, response = pi_tre.send_document_by_id(document_id=document_id)
    if status == 1:
        raise PiTreException(response)
    logger.info(response)


def get_project_template(document):
    """
    Method for the construction of specific fields of a typed project given a document object
    :param document: Document object
    :return: Populated project template
    """
    base_template = pi_tre.project_template
    template = {
        "Id": base_template["Id"],
        "Name": base_template["Name"],
        "Fields": []
    }
    for field in base_template["Fields"]:
        field_name = field["Name"]
        # Override project custom configuration
        _filter = {
            "Name": field_name,
            "Value": None
        }
        if "project" in document["meta"] and field_name in document["meta"]["project"]:
            _filter["Value"] = document["meta"]["project"][field_name]
        elif field_name in document["meta"].keys():
            _filter["Value"] = document["meta"][field_name]

        if _filter["Value"]:
            template["Fields"].append(_filter)

    return template


def get_typed_project_search_filters(document):
    """
    Method for constructing the filters needed to search for a typed project
    :param document: Document object
    :return: PiTre Template filters
    """
    base_template = pi_tre.project_template
    template_filter = {
        "Name": "TEMPLATE",
        "Value": base_template["Id"],
        "Template": get_project_template(document)
    }
    return template_filter


def get_document_template(document):
    """
        Method for the construction of specific fields of a typed document given a document object
        :param document: Document object
        :return: Populated document template
        """
    base_template = pi_tre.get_document_template(income=document["type"] == INCOME_TYPE)
    template = {
        "Id": base_template["Id"],
        "Name": base_template["Name"],
        "Fields": []
    }
    for field in base_template["Fields"]:
        field_name = field["Name"]
        if field_name in document["meta"].keys():
            template["Fields"].append({
                "Name": field_name,
                "Value": document["meta"][field_name]
            })
    return template


def get_typed_document_search_filters(document):
    """
       Method for constructing the filters needed to search for a typed document
       :param document: Document object
       :return: PiTre Template filters
       """
    base_template = pi_tre.get_document_template(income=document["type"] == INCOME_TYPE)
    template_filter = {
        "Name": "TEMPLATE",
        "Value": base_template["Id"],
        "Template": get_document_template(document)
    }
    return template_filter


def update_source(document):
    """
    Method for updating source protocol data
    :param document: registered document
    """

    if not document["signature"]:
        # Retrieve document for signature
        status, response = pi_tre.get_document(document_id=document["document_id"])
        if status == 1:
            raise PiTreException(response)
        document["document_id"] = response["Id"]
        document["signature"] = response["Signature"]
        # Retrieve project
        status, response = pi_tre.get_projects_by_document(id_document=document["document_id"])
        if status == 1:
            raise PiTreException(response)
        document["project_id"] = response[0]["Id"]
        document["project_code"] = response[0]["Code"]

    # Get protocol timestamp
    status, response = pi_tre.get_timestamp_and_signature(id_document=document["document_id"])
    if status == 1:
        raise PiTreException(response)
    logger.debug(json.dumps(response, indent=2))
    document["protocol_datetime"] = f"{response['DataProtocol']} {response['TimeProtocol']}"
    SDC.update_source(document)


pi_tre = None
# pitre initialization
try:
    logger.info("Setup PiTre configuration...")
    pi_tre = PiTre(
        certificate=config['pi_tre']['certificate'],
        api_url=config['pi_tre']['api_url'],
        username=config['pi_tre']['username'],
        code_role=config['pi_tre']['code_role'],
        code_application=config['pi_tre']['code_application'],
        code_adm=config['pi_tre']['code_adm'],
        code_register=config['pi_tre']['code_register'],
        means_of_sending=config['pi_tre']['means_of_sending'],
        code_rf=config['pi_tre']['code_rf'],
        default_sender=config['pi_tre']['default_sender'],
        transmission_reason=config['pi_tre']['transmission_reason'],
        transmission_project_models=config['pi_tre']['transmission_project_model'],
        transmission_document_models=config['pi_tre']['transmission_document_model'],
        code_node_classification=config['pi_tre']['code_node_classification'],
        receiver_code=config['pi_tre']['receiver_code'],
        default_note=config['pi_tre']['default_note'],
        address_book_rf=config['pi_tre']['address_book_rf'],
        income_document_template_id=config['pi_tre']['income_document_template_id'],
        outcome_document_template_id=config['pi_tre']['outcome_document_template_id'],
        project_template_id=config['pi_tre']['project_template_id'],
        private_project=config['pi_tre']['private_project'] if 'private_project' in config['pi_tre'] else None,
        private_document=config['pi_tre']['private_document'] if 'private_document' in config['pi_tre'] else None,
        connection_timeout=CONNECTION_TIMEOUT,
        read_timeout=READ_TIMEOUT,
        sleep_seconds=ERROR_SLEEP_SECONDS
    )

    if TYPED_PROJECT and not pi_tre.project_template:
        raise Exception("Typed project  is enabled but no template is defined")
    if TYPED_DOCUMENT and not (pi_tre.income_document_template or pi_tre.outcome_document_template):
        raise Exception("Typed document is enabled but no template is defined")
    if TRANSMIT_DOCUMENT and not pi_tre.transmission_document_models:
        raise Exception("Document transmission is enabled but no model is defined")

    logger.info("Setup completed")

except Exception as ex:
    logger.error(str(ex))
    if LOG_LEVEL == "DEBUG":
        traceback.print_exc()
    exit(1)

documents = None
registered_documents = 0

try:
    logger.info("Setup SDC configuration...")
    # SDC initialization
    SDC = SDC(
        api_url=config['sdc']['api_url'],
        username=config['sdc']['username'],
        password=config['sdc']['password'],
        logo_url=config['sdc']['logo_url'],
        tenant=config['sdc']['tenant'],
        api_version=API_VERSION,
        mapping=json.load(open("config/mapping.json")),
        statuses_whitelist=STATUSES_WHITELIST,
        services_whitelist=SERVICES_WHITELIST,
        ids=IDS,
        record_statuses=RECORD_STATUSES,
        record_responses=RECORD_RESPONSES,
        record_notes=RECORD_NOTES,
        record_messages=RECORD_MESSAGES,
        gotenberg_url=GOTENBERG_URL,
        save_pdfs=SAVE_PDFS,
        save_xmls=SAVE_XMLS,
        connection_timeout=CONNECTION_TIMEOUT,
        read_timeout=READ_TIMEOUT,
        sleep_seconds=ERROR_SLEEP_SECONDS
    )

    logger.info("Setup completed")

    # Get documents from SDC applications
    logger.info('Retrieving documents...')
    documents = SDC.get_documents_to_protocol(
        offset=API_START_OFFSET,
        limit=API_LIMIT,
        order=API_ORDERING,
        sort=API_SORTING
    )

except Exception as ex:
    logger.error(str(ex))
    if LOG_LEVEL == "DEBUG":
        traceback.print_exc()
    exit(1)

# PROTOCOL FLOW
for document in documents:
    # Initialization
    pitre_project = None
    pitre_document = None
    pitre_correspondent = None

    # Redis keys for projects, correspondences and documents
    redis_project_key = "Project {}".format(document["project_object"])
    redis_correspondent_key = "Correspondent {}".format(document["correspondent"]["fiscal_code"])
    redis_document_key = "Document {}".format(document["document_object"])
    redis_registered_key = "Registered {} - {}".format(document["project_object"], document["document_object"])
    redis_last_time_key = "last_document_time"
    redis_error_key = "Error {}".format(document["document_object"])

    # Check whether document has already been registered
    registered_document = redis.get(redis_registered_key)
    if registered_document:
        pitre_document_id = int(registered_document)
        # Redis key exists, document has already been registered => Skip
        logger.info(f'Document "{document["document_object"]}" has already been registered: {pitre_document_id}')

        if not document["signature"] and UPDATE_SOURCE:
            logger.info("Update missing data on source")
            document["document_id"] = pitre_document_id
            update_source(document)
            logger.info("Update completed")

        logger.info("\n=============================================================================================\n")
        continue
    try:
        if document["signature"]:
            logger.info(f'Document "{document["document_object"]}" has already been registered: {document["signature"]}')
            continue

        logger.info(f'Protocol document "{document["document_object"]}"')

        # --------------------------------------------------- PROJECT --------------------------------------------------
        logger.info("Retrieve project\n")

        # Check whether pitre project has already been created
        pitre_project = redis.get(redis_project_key)
        if not pitre_project:
            # Redis key does not exists, search project on pitre and create it if does not exists
            pitre_project = search_or_create_project(document)
            # Save newly created project inside redis
            redis_mset(redis_project_key, json.dumps(pitre_project))
        else:
            # Redis key exists, project has already been created
            pitre_project = json.loads(pitre_project)

        logger.debug(json.dumps(pitre_project, indent=2))

        # ------------------------------------------------ CORRESPONDENT -----------------------------------------------
        logger.info("Retrieve correspondent\n")

        if SEARCH_CORRESPONDENT:
            # Search correspondence in pitre address book is enabled
            # Check whether pitre correspondence has already been created
            pitre_correspondent = redis.get(redis_correspondent_key)
            if not pitre_correspondent:
                # Redis key does not exists, search correspondence on pitre and create it if does not exists
                pitre_correspondent = search_or_create_correspondent(document)
                # Save newly created correspondence inside redis
                redis_mset(redis_correspondent_key, json.dumps(pitre_correspondent))
            else:
                # Redis key exists, correspondence has already been created
                pitre_correspondent = json.loads(pitre_correspondent)
        else:
            # Search correspondence is not enabled => create occasional correspondence
            pitre_correspondent = pi_tre.create_occasional_correspondent(
                description=document["correspondent"]["description"]
            )

        logger.debug(json.dumps(pitre_correspondent, indent=2))

        # --------------------------------------------------- DOCUMENT -------------------------------------------------
        logger.info("Retrieve document\n")

        # Check whether pitre document has already been created
        pitre_document = redis.get(redis_document_key)
        if not pitre_document:
            # Redis key does not exists, search document on pitre and create it if does not exists
            pitre_document = search_or_create_document(document, pitre_correspondent)
            # Save newly created document inside redis
            redis_mset(redis_document_key, json.dumps(pitre_document))
        else:
            # Redis key exists, document has already been created
            pitre_document = json.loads(pitre_document)

        logger.debug(json.dumps(pitre_document, indent=2))
        # ------------------------------------------------ MAIN DOCUMENT -----------------------------------------------
        logger.info("Upload main document\n")

        # Check whether main document has already been uploaded
        if not pitre_document["MainDocument"] or pitre_document["MainDocument"]["Name"] is None:
            # Document does not have a main document => Upload main document file
            pitre_outcome = upload_file(document["document"], pitre_document["Id"], False)
            logger.info(pitre_outcome)

            # Retrieve updated document in pitre and save in redis
            pitre_document = get_updated_document(pitre_document["Id"])
            redis_mset(redis_document_key, json.dumps(pitre_document))

            logger.debug(json.dumps(pitre_document, indent=2))
        else:
            # Main document file has already been uploaded
            logger.info("Main module already uploaded")

        # ------------------------------------------------- ATTACHMENTS ------------------------------------------------
        logger.info("Upload attachments\n")
        pitre_document = get_updated_document(pitre_document["Id"])
        # Check whether attachments have already been uploaded
        if not pitre_document["Attachments"]:
            # Pitre document has no attachments => upload attachments
            for attachment in document["attachments"]:
                pitre_outcome = upload_file(attachment, pitre_document["Id"])
                logger.info(pitre_outcome)

                # Retrieve updated document in pitre and save in redis
                pitre_document = get_updated_document(pitre_document["Id"])
                redis_mset(redis_document_key, json.dumps(pitre_document))
        else:
            # Some attachments have already been uploaded
            for attachment in document["attachments"]:
                # Check attachments names to upload missing attachments if needed
                uploaded = False
                for pitre_attachment in pitre_document["Attachments"]:
                    if attachment["original_name"] == pitre_attachment["Name"]:
                        logger.info(f'Attachment "{attachment["original_name"]}" has already been uploaded')
                        uploaded = True
                    elif attachment["name"] == pitre_attachment["Name"]:
                        logger.info(f'Attachment "{attachment["name"]}" has already been uploaded')
                        uploaded = True
                if not uploaded:
                    # Missing attachment
                    pitre_outcome = upload_file(attachment, pitre_document["Id"])
                    logger.info(pitre_outcome)

                    # Retrieve updated document in pitre and save in redis
                    pitre_document = get_updated_document(pitre_document["Id"])
                    redis_mset(redis_document_key, json.dumps(pitre_document))

        logger.debug(json.dumps(pitre_document, indent=2))

        # --------------------------------------------------- PROTOCOL -------------------------------------------------
        logger.info("Protocol document\n")

        # Check whether document signature has already been generated
        if not pitre_document['Signature']:
            # Missing signature, protocol document
            pitre_document, pitre_outcome = protocol_document(pitre_document["Id"], pitre_project["Code"])
            logger.info(pitre_outcome)

            # Save updated document inside redis
            redis_mset(redis_document_key, json.dumps(pitre_document))

            registered_documents += 1

        # Save registered document id in redis
        redis_mset(redis_registered_key, pitre_document["Id"])

        if TRANSMIT_DOCUMENT:
            logger.info("Transmitting document \"{}\"".format(document["document_object"]))
            transmit_document(pitre_document["Id"])

        if SEND_OUTCOME_DOCUMENT and document["type"] == OUTCOME_TYPE:
            logger.info("Sending document \"{}\"".format(document["document_object"]))
            pitre_outcome = send_document(pitre_document["Id"])
            logger.info(pitre_outcome)

        # Save arrival date of last registered document
        redis_mset(redis_last_time_key, document["arrival_date"])

        # Add csv line
        log_to_file(
            writer=output_writer,
            application_id=document["meta"]["external_key"],
            project=pitre_project["Code"],
            project_description=document["project_object"],
            document_id=pitre_document["Id"],
            document_description=document["document_object"],
            protocol_number=pitre_document["ProtocolNumber"],
            signature=pitre_document["Signature"],
            error=None
        )
        output_file.flush()

        # -------------------------------------------------- SDC UPDATE ------------------------------------------------
        if UPDATE_SOURCE:
            # Update source functionality is enabled, update SDC application with protocol data
            logger.info("Update source with protocol data: {}".format(json.dumps({
                "project_id": pitre_project["Id"],
                "document_id": pitre_document["Id"],
                "signature": pitre_document["Signature"]
            }, indent=2)))
            document["project_id"] = pitre_project["Id"]
            document["project_code"] = pitre_project["Code"]
            document["document_id"] = pitre_document["Id"]
            document["signature"] = pitre_document["Signature"]
            update_source(document)
            logger.info("Update completed")

        # If number of documents to register has been limited check limit
        if 0 < LIMIT <= registered_documents:
            logger.info("Limit of {} documents has been reached".format(LIMIT))
            break

        logger.info("\n=============================================================================================\n")

        time.sleep(SUCCESS_SLEEP_SECONDS)
    except Exception as ex:
        # Error: log error and skip to next document
        logger.error(str(ex))
        if LOG_LEVEL == "DEBUG":
            traceback.print_exc()

        error = redis.get(redis_error_key)
        if error:
            error = error.decode()

        if not error or error != str(ex):
            # If this error has already been saved log it
            redis_mset(redis_error_key, str(ex))
            log_to_file(
                writer=output_writer,
                application_id=document["meta"]["external_key"],
                project=pitre_project["Code"] if pitre_project else None,
                project_description=document["project_object"],
                document_id=pitre_document["Id"] if pitre_document else None,
                document_description=document["document_object"],
                protocol_number=pitre_document["ProtocolNumber"] if pitre_document else None,
                signature=pitre_document["Signature"] if pitre_document else None,
                error=str(ex)
            )
            output_file.flush()

        time.sleep(ERROR_SLEEP_SECONDS)

logger.info("{} documents have been registered. Exiting...".format(registered_documents))
