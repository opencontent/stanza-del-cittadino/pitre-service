import copy
import datetime
import errno
import hashlib
import json
import os
import re
from time import sleep
import xml.etree.ElementTree as gfg
import xml.dom.minidom
import requests

from utils.logger import logger

DOCUMENT_TYPES = ["document", "outcome", "message", "user_message", "operator_message"]
INCOME_TYPE = "IN"
OUTCOME_TYPE = "OUT"

# Specific Document types
APPLICATION_TYPE = "application"
MESSAGE_TYPE = "message"
RECORD_TYPES = ["always", "never", "on-demand"]

DEFAULT_CORRESPONDENT = {
    "description": "%applicant.data.completename.data.surname% %applicant.data.completename.data.name% - %applicant.data.fiscal_code.data.fiscal_code%",
    "name": "%applicant.data.completename.data.name%",
    "surname": "%applicant.data.completename.data.surname%",
    "fiscal_code": "%applicant.data.fiscal_code.data.fiscal_code%",
    "code": "%applicant.data.fiscal_code.data.fiscal_code%",
    "phone_number": "",
    "phone_number2": "",
    "email": "",
    "other_emails": "",
    "address": "",
    "cap": "",
    "city": "",
    "province": "",
    "location": "",
    "nation": "",
    "fax": "",
    "vat_number": "",
    "note": ""
}


class SDCException(Exception):
    pass


def merge_dict(x, y):
    """
    Method for joining two dictionaries
    :param x: Main dictionary
    :param y: Secondary dictionary to merge
    :return: Merged dictionary
    """

    # start with y's keys and values
    z = y.copy()
    z.update(x)
    return z


class SDC:
    def __init__(self, api_url, username, password, logo_url, tenant, gotenberg_url, mapping, api_version=1, attempts=10,
                 sleep_seconds=60, connection_timeout=10, read_timeout=300,
                 statuses_whitelist=None, services_whitelist=None, ids=None, record_statuses=None,
                 record_responses=True, record_notes="never", record_messages="never",
                 save_pdfs=False, save_xmls=False):

        if record_statuses is None:
            record_statuses = []
        if ids is None:
            ids = []
        if services_whitelist is None:
            services_whitelist = []
        if statuses_whitelist is None:
            statuses_whitelist = []

        self.token = None

        self.api_url = api_url
        self.username = username
        self.password = password
        self.logo_url = logo_url
        self.tenant = tenant
        self.mapping = mapping
        self.api_version = str(api_version)
        self.gotenberg_url = gotenberg_url

        self.record_statuses = record_statuses
        self.record_notes = record_notes
        self.record_messages = record_messages
        self.record_responses = record_responses
        self.statuses_whitelist = statuses_whitelist
        self.services_whitelist = services_whitelist
        self.ids = ids

        self.attempts = attempts
        self.sleep_seconds = sleep_seconds
        self.connection_timeout = connection_timeout
        self.read_timeout = read_timeout

        self.save_pdfs = save_pdfs
        self.save_xmls = save_xmls

        if record_notes not in RECORD_TYPES:
            raise SDCException("SDC: invalid record_notes type {}".format(record_notes))

        if record_messages not in RECORD_TYPES:
            raise SDCException("SDC: invalid record_messages type {}".format(record_notes))

        self.set_token()

    def set_token(self):
        """
        Method for generating an authentication token
        :return: "SDC Bearer token"
        """
        attempt = 0
        data = {
            "username": self.username,
            "password": self.password
        }
        url = "{}/auth".format(self.api_url)

        while attempt < self.attempts:
            attempt += 1
            try:
                response = requests.post(
                    url=url,
                    json=data,
                    timeout=(self.connection_timeout, self.read_timeout),
                    headers={"X-Accept-Version": self.api_version}
                )
            except requests.exceptions.ConnectionError:
                logger.error(f"SDC: unable to connect to url {url} after {attempt} attempts")
            except requests.exceptions.ReadTimeout:
                logger.error(f"SDC: unable to read from url {url} after {attempt} attempts")
            except Exception as ex:
                logger.error(f"SDC: An error occurred from url {url} after {attempt} attempts: {str(ex)}")
            else:
                if response.ok:
                    self.token = f"Bearer {response.json()['token']}"
                    return
                if response.status_code not in [429, 502, 503, 504]:
                    try:
                        error = json.dumps(response.json(), indent=4)
                    except json.decoder.JSONDecodeError:
                        # no json response
                        error = response.text
                    raise SDCException(f"SDC: Get {response.status_code} error while retrieving token:\n{error}")
                else:
                    logger.error(f"SDC: {response.status_code} error while retrieving token after {attempt} attempts")
                    sleep(pow(self.sleep_seconds, attempt))
        # reached max attempts
        raise SDCException(f"SDC: cannot get token after {attempt} attempts")

    def replace_placeholders(self, string, application, uppercase=True):
        """
        Method for replacing placeholders with specific fields of an application
        :param uppercase: convert replaced data in uppercase: default True. NB only data extracted from form data will be converted
        :param string: Phrase containing placeholders
        :param application: Application from which to obtain the values of the placeholders to be replaced
        :return: Initial phrase to which the placeholders have been replaced (if a matching has been found in the
        application)
        """
        # Get placeholders inside phrase
        placeholders = re.findall(r"%[A-Za-z.\-_1-9]+%", string)

        for placeholder in placeholders:
            clean_placeholder = placeholder.replace("%", "")
            if clean_placeholder in application:
                # Search placeholder value in application keys
                string = string.replace(placeholder, str(application[clean_placeholder]))

            if clean_placeholder in application["data"]:
                # Search placeholder value in application module keys
                value = str(application["data"][clean_placeholder])
                string = string.replace(placeholder, value.upper() if uppercase else value)

            if clean_placeholder in application["authentication"]:
                # Search placeholder value in application module keys
                string = string.replace(placeholder, str(application["authentication"][clean_placeholder]) if
                application["authentication"][clean_placeholder] else "")

            # Conventions
            if clean_placeholder == "authentication.instant" and "instant" in application["authentication"] \
                    and application["authentication"]["instant"]:
                instant = datetime.datetime.strptime(application["authentication"]["instant"], "%Y-%m-%dT%H:%M:%S.%f%z")
                # Search placeholder value in application authentication keys
                string = string.replace(placeholder, instant.strftime("%d/%m/%Y alle ore %H:%M:%S"))

            if clean_placeholder == "application.year":
                string = string.replace(placeholder, str(
                    datetime.datetime.strptime(
                        application["submitted_at"],
                        "%Y-%m-%dT%H:%M:%S%z"
                    ).strftime("%Y")
                ))

            if clean_placeholder == 'message.datetime' and "message_created_at" in application:
                string = string.replace(
                    "%message.datetime%",
                    datetime.datetime.strptime(
                        application["message_created_at"],
                        "%Y-%m-%dT%H:%M:%S%z"
                    ).strftime("%d/%m/%Y alle %H:%M:%S"))

            # Remove placeholder if not found
            string = string.replace(placeholder, "")
            # Remove double spaces
            string = string.replace("  ", " ")

        return string

    def safe_request(self, url, content=False, stream=False):
        """
        Method for handling possible errors that may be encountered in GET a http request
        :param url: url to request
        :param content: return type is content? Default json
        :param stream: return stream
        :return: Response data
        """
        attempt = 0
        headers = {
            "Authorization": self.token,
            "X-Accept-Version": self.api_version
        }

        # Retry request for #attempts
        while attempt < self.attempts:
            attempt += 1
            try:
                response = requests.get(
                    url=url,
                    headers=headers,
                    stream=stream,
                    timeout=(self.connection_timeout, self.read_timeout)
                )
            except requests.exceptions.ConnectionError:
                logger.error(f"SDC: Unable to connect to url {url} after {attempt} attempts")
            except requests.exceptions.ReadTimeout:
                logger.error(f"SDC: Unable to read from url {url} after {attempt} attempts")
            except Exception as ex:
                logger.error(f"SDC: An error occurred from url {url} after {attempt} attempts: {str(ex)}")
            else:
                if response.ok:
                    if content:
                        return {"content": response.content}
                    if stream:
                        return response
                    return response.json()

                if response.status_code == 401:
                    logger.error(f"SDC: Token expired: request a new one after {attempt} attempts")
                    # If token is expired, request a new one
                    self.set_token()
                elif response.status_code not in [429, 502, 503, 504]:
                    try:
                        error = json.dumps(response.json(), indent=4)
                    except json.decoder.JSONDecodeError:
                        # no json response
                        error = response.text
                    raise SDCException(f"SDC: {response.status_code} error while fetching {url}:\n{error}")
                else:
                    logger.error(f"SDC: {response.status_code} error while fetching {url} after {attempt} attempts")
                    sleep(pow(self.sleep_seconds, attempt))

        # reached max attempts
        raise SDCException(f"SDC: Cannot complete request {url} after {attempt} attempts")

    def safe_update(self, url, data):
        """
        Method for handling possible errors that may be encountered in PATCH a http request
        :param url: Url of the resource to update
        :param data: Updated data
        :return: Response data
        """
        attempt = 0
        headers = {
            "Authorization": self.token,
            "X-Accept-Version": self.api_version
        }

        # Retry update for #attempts
        while attempt < self.attempts:
            attempt += 1
            try:
                response = requests.patch(
                    url=url,
                    headers=headers,
                    data=json.dumps(data),
                    timeout=(self.connection_timeout, self.read_timeout)
                )
            except requests.exceptions.ConnectionError:
                logger.error(f"SDC: unable to connect to url {url} after {attempt} attempts")
            except requests.exceptions.ReadTimeout:
                logger.error(f"SDC: unable to read from url {url} after {attempt} attempts")
            except Exception as ex:
                logger.error(f"SDC: An error occurred from url {url} after {attempt} attempts: {str(ex)}")
            else:
                if response.ok:
                    return response.json()

                if response.status_code == 401:
                    logger.error(f"SDC: Token expired: request a new one after {attempt} attempts")
                    # If token is expired, request a new one
                    self.set_token()
                elif response.status_code not in [429, 502, 503, 504]:
                    try:
                        error = json.dumps(response.json(), indent=4)
                    except json.decoder.JSONDecodeError:
                        # no json response
                        error = response.text
                    raise SDCException(
                        f"SDC: Get {response.status_code} error while updating {url} with data {json.dumps(response.json(), indent=2)}:\n{error}")
                else:
                    logger.error(f"SDC: {response.status_code} error while fetching {url} after {attempt} attempts")
                    sleep(pow(self.sleep_seconds, attempt))

        # reached max attempts
        raise SDCException(f"SDC: cannot complete update {url} after {attempt} attempts")

    def safe_gotenberg_request(self, content):
        """
        Method for handling possible errors that may be encountered in Gotenberg request
        :param content: content to be generated using Gotenberg
        :return: Gotenberg response content
        """
        attempt = 0

        while attempt < self.attempts:
            attempt += 1
            try:
                response = requests.post(
                    url=self.gotenberg_url,
                    files={'file': ('index.html', content)},
                    data={
                        "marginTop": 0,
                        "marginLeft": 0,
                        "marginBottom": 0,
                        "marginRight": 0,
                    },
                    timeout=(self.connection_timeout, self.read_timeout)
                )
            except requests.exceptions.ConnectionError:
                logger.error(f"SDC: unable to connect to url {self.gotenberg_url} after {attempt} attempts")
            except requests.exceptions.ReadTimeout:
                logger.error(f"SDC: unable to read from url {self.gotenberg_url} after {attempt} attempts")
            except Exception as ex:
                logger.error(
                    f"SDC: An error occurred from url {self.gotenberg_url} after {attempt} attempts: {str(ex)}")
            else:
                if response.ok:
                    return response.content
                elif response.status_code not in [429, 502, 503, 504]:
                    try:
                        error = json.dumps(response.json(), indent=4)
                    except json.decoder.JSONDecodeError:
                        # no json response
                        error = response.text
                    raise SDCException(f"SDC: {response.status_code} error while generating PDF document:\n{error}")
                else:
                    logger.error(f"SDC: {response.status_code} error while generating PDF document after {attempt} attempts")
                    sleep(pow(self.sleep_seconds, attempt))

        # reached max attempts
        raise SDCException(f"SDC: cannot generate PDF document {attempt} attempts")

    def get_documents_to_protocol(self, offset=None, limit=None, order=None, sort=None):
        """
        Method for searching and building all documents relating to the applications that must be registered
        :param offset: SDC api start offset
        :param limit: SDC api page limit
        :param order: SDC api ordering
        :param sort: SDC api sorting
        :return: List of all documents to be registered
        """
        documents = []

        # Get applications to be registered
        applications = self.get_applications(
            offset=offset,
            limit=limit,
            order=order,
            sort=sort
        )
        for application in applications:
            # Extract all documents to be registered from a given appliaction
            documents += self.get_documents_from_application(application=application)

        # Sort documents by arrival time
        return sorted(documents, key=lambda i: i['arrival_date'])

    def get_applications(self, offset=None, limit=None, order=None, sort=None):
        """
        Method for the recovery of the applications to be registered
        :param offset: SDC api start offset (default: 0 )
        :param limit: SDC api page limit (default: 100 )
        :param order: SDC api ordering (default: submissionTime )
        :param sort: SDC api sorting (default: ASC )
        :return: List of all applocations to be registered
        """
        applications = []

        # Check if an application's message retrieval is required
        # The recovery is not necessary if both parameters "RECORD_MESSAGES" and "RECORD_NOTES" have the value "never"
        add_messages = True if self.record_messages != "never" or self.record_notes != "never" else False

        url = "{}/applications?offset={}&limit={}&order={}&sort={}".format(
            self.api_url,
            offset or 0,
            limit or 100,
            order or "submissionTime",
            sort or "ASC"
        )

        if len(self.services_whitelist) == 1:
            url = f"{url}&service={self.services_whitelist[0]}"

        while url:
            # Scroll through the API pages to retrieve all the applications
            logger.info(url)
            _applications = self.get_from_url(url)

            # Next page link
            url = _applications["links"]["next"]

            if not (self.statuses_whitelist or self.services_whitelist or self.ids or add_messages):
                # No filter on applications must be applied: if it is not necessary to retrieve messages/notes
                # Simply add the applications to the list
                applications += _applications["data"]
            else:
                # At least one filter is required
                _application: dict
                for _application in _applications["data"]:
                    if not self.ids:
                        # If a list of ids has not been specified, check the status and service of the application
                        if self.statuses_whitelist and _application["status_name"] not in self.statuses_whitelist:
                            continue
                        if self.services_whitelist and _application["service"] not in self.services_whitelist:
                            continue
                    elif _application["id"] not in self.ids:
                        # If the list of ids is present, it is assumed that no further filter is needed
                        continue

                    # Initialize application messages
                    messages = []
                    status_messages = []

                    # List of status messages to ignore when retrieving messages: in this way only messages not associated
                    # with an application status change will be registered
                    ignore_messages = []

                    # Get status change messages from application history
                    application_history = self.get_application_history(_application['id'])
                    status: dict
                    for status in application_history:
                        message_id = status['message_id']
                        if message_id:
                            # Ignore list: ignore this message when retrieving all application messages
                            ignore_messages.append(message_id)
                            if status['status_name'] in self.record_statuses:
                                # Check if status message has to be sent to the protocol system
                                message = self.get_application_message(_application['id'], message_id)
                                if type(message) == list:
                                    message = message[0]
                                status_messages.append(message)

                    if add_messages:
                        # Retrieve application messages
                        application_messages = self.get_application_messages(_application['id'])
                        message: dict
                        for message in application_messages:
                            if message['id'] not in ignore_messages:
                                messages.append(message)

                    _application["status_messages"] = status_messages
                    _application["messages"] = messages

                    applications.append(_application)

        return applications

    def get_application(self, id):
        """
        Get an application given its identifier
        :param id: Application id
        :return: Application data
        """
        url = "{}/applications/{}".format(self.api_url, id)
        return self.safe_request(url)

    def get_application_messages(self, id):
        """
        Get all application messages given the application identifier
        :param id: application id
        :return: List of application's messages
        """
        url = "{}/applications/{}/messages".format(self.api_url, id)
        return self.safe_request(url)

    def get_application_message(self, id, message_id):
        """
        Get an application message given the application identifier and the message id
        :param id: application id
        :param message_id: message id
        :return: List of application's messages
        """
        url = "{}/applications/{}/messages/{}".format(self.api_url, id, message_id)
        return self.safe_request(url)

    def get_application_history(self, id):
        """
        Get application history given the application identifier
        :param id: application id
        :return: List of application's messages
        """
        url = "{}/applications/{}/history".format(self.api_url, id)
        return self.safe_request(url)

    def get_from_url(self, url, content=False):
        """
        Get data from url
        :param url: Requested url
        :param content: If content is set to "true" the return type is of type content (default json)
        :return: Requested data
        """
        return self.safe_request(url, content)

    def get_attachment_data(self, attachment):
        """
        Extrapolation of the data necessary for the registration of the application's attachments
        :param attachment: SDC attachment data
        :return: dictionary containing all data necessary for the retrieval and registration af the attachment
        """
        return {
            "url": attachment["url"],
            "name": attachment["description"],
            "original_name": attachment['originalName'] if "originalName" in attachment else attachment["name"],
        }

    def get_authentication_data(self, application):
        if "authentication" not in application:
            return None

        if not application["authentication"]["session_id"]:
            return None

        instant = None
        if "instant" in application["authentication"] and application["authentication"]["instant"]:
            instant = datetime.datetime.strptime(application["authentication"]["instant"], "%Y-%m-%dT%H:%M:%S.%f%z")

        certificate = None
        if "certificate" in application["authentication"] and application["authentication"]["certificate"]:
            certificate = bytes(application["authentication"]["certificate"], encoding="utf-8")

        security = None
        if "session_id" in application["authentication"]:
            security = application["authentication"]["session_id"]

        spid_code = None
        if "spid_code" in application["authentication"]:
            spid_code = application["authentication"]["spid_code"]

        authentication_method = None
        # Fixme: check on API data
        if application["authentication"]["authentication_method"] == "spid":
            authentication_method = "OTP"

        if application["authentication"]["authentication_method"] == "cps/cns":
            authentication_method = "SMARTCARD"

        auth_data = {
            "application_id": application["id"],
            "service_provider": application["authentication"]["authentication_method"].upper(),
            "certification_authority": application["authentication"]["certificate_issuer"],
            "fiscal_code": application["data"]['applicant.data.fiscal_code.data.fiscal_code'].upper(),
            "surname": application["data"]['applicant.data.completename.data.surname'].upper(),
            "name": application["data"]['applicant.data.completename.data.name'].upper(),
            "authentication_date": instant.strftime("%d/%m/%Y") if instant else None,
            "authentication_time": instant.strftime("%H:%M") if instant else None,
            "certificate_release_date": None,
            "certificate_due_date": None,
            "certificate_hash": hashlib.sha256(certificate).hexdigest() if certificate else None,
            "certificate_number": None,
            "security": security,
            "module_id": "STANZA DEL CITTADINO / {} / {}".format(self.get_service_name(application),
                                                                 application["id"]).upper(),
            "spid_code": spid_code,
            "authentication_method": authentication_method,
            "hash_data": None,
            "hash_document": self.get_remote_hash(application["compiled_modules"][0]["url"])
        }
        return auth_data

    def get_project_name(self, application):
        """
        Construction of the project object that defines the Pitre project in which to insert the registered documents
        of an application.
        This function makes use of the configuration defined inside the mapping variable to differentiate the
        services and the type of document
        :param application: Application from which construct the project object
        :return: PiTre project object
        """
        service = application["service"]
        if service not in self.mapping["services"]:
            raise SDCException("SDC: Missing service {}".format(service))

        group = self.mapping["services"][service]["group"]
        if group:
            # Service in defined in a service group
            if group not in self.mapping["service_groups"]:
                raise SDCException("SDC: Missing service group {}".format(group))
            return self.replace_placeholders(self.mapping["service_groups"][group]["project_object"], application)
        # Standalone service
        return self.replace_placeholders(self.mapping["services"][service]["project_object"], application)

    def get_document_name(self, application, type):
        """
        Construction of the document object that defines the Pitre document to protocol
        This function makes use of the configuration defined inside the mapping variable to differentiate the
        services and the type of document
        :param application: Application from which construct the document object
        :param type: Document type (application/response/message)
        :return:  PiTre document object
        """
        service = application["service"]

        if service not in self.mapping["services"]:
            raise SDCException("SDC: Missing service {}".format(service))
        # Check if properties must be overwritten
        service_config = self.mapping["services"][service]
        if type in service_config and "document_object" in service_config[type]:
            return self.replace_placeholders(self.mapping["services"][service][type]["document_object"], application)
        return self.replace_placeholders(self.mapping["services"][service]["document_object"], application)

    def get_correspondent(self, application):
        service = application["service"]

        if service not in self.mapping["services"]:
            raise SDCException("SDC: Missing service {}".format(service))
        service_config = self.mapping["services"][service]

        if "correspondent" in service_config:
            correspondent = merge_dict(service_config["correspondent"], DEFAULT_CORRESPONDENT)
        else:
            correspondent = DEFAULT_CORRESPONDENT.copy()

        # Replace placeholders
        for key in correspondent.keys():
            correspondent[key] = self.replace_placeholders(
                correspondent[key],
                application,
                key not in ["email", "other_emails"]
            )

        correspondent["other_emails"] = correspondent["other_emails"].split("|") if correspondent[
            "other_emails"] else []

        return correspondent

    def get_metadata(self, application, type):
        """
        Method for the construction of the document metadata necessary in the case of typed projects and typed documents
        for the population of the fields of the PiTre template
        This function makes use of the configuration defined inside the mapping variable to differentiate the
        services and the type of document
        :param application: Application from which construct the metadata
        :return: Dictionary containing the valued metadata defined in the mapping.json variable
        """
        service = application["service"]
        if service not in self.mapping["services"]:
            raise SDCException("SDC: Missing service {}".format(service))
        meta = {}

        service_config = self.mapping["services"][service]

        # Check if metadata must be overwritten
        if type in service_config and "meta" in service_config[type]:
            _meta = merge_dict(service_config[type]["meta"], service_config["meta"])
        else:
            _meta = service_config["meta"]

        for key in _meta:
            # replace metadata placeholders
            meta[key] = self.replace_placeholders(_meta[key], application)

        if "project" in service_config and "meta" in service_config["project"]:
            _meta = service_config["project"]["meta"]
            meta["project"] = {}
            for key in _meta:
                # replace metadata placeholders
                meta["project"][key] = self.replace_placeholders(_meta[key], application)

        if "external_key" not in meta:
            meta["external_key"] = application["id"]

        return meta

    def get_service_name(self, application):
        """
        Retrieving the full name of the service to which the application belongs
        :param application:
        :return:
        """
        service = application["service"]
        if service not in self.mapping["services"]:
            raise SDCException("SDC: Missing service {}".format(service))
        return self.mapping["services"][service]["name"]

    def get_main_document(self, application, template):
        """
        Construction of a "compiled module" type document
        :param application: application to protocol
        :param template: base document template
        :return: compiled module document
        """
        document = copy.deepcopy(template)
        document["document_object"] = self.get_document_name(application, "document")
        document["document"] = self.get_attachment_data(application["compiled_modules"][0])
        document["type"] = INCOME_TYPE
        document["arrival_date"] = application["submitted_at"]
        document["source_type"] = APPLICATION_TYPE
        document["source_url"] = "{}/applications/{}".format(self.api_url, application["id"])
        document["meta"] = self.get_metadata(application, "document")
        document["signature"] = application["protocol_number"]

        for attachment in application["attachments"]:
            # protocol all attachments if no field, else check flag value
            if "protocol_required" not in attachment or attachment["protocol_required"]:
                document["attachments"].append(self.get_attachment_data(attachment))
            else:
                logger.debug(f"Attachment {attachment['name']} skipped: protocol is not required")

        if "authentication" in application and application["authentication"]["session_id"]:
            document["attachments"].append(self.generate_authentication_attachment(application=application))

        return document

    def get_response_document(self, application, template):
        """
        Construction of a "response" type document
        :param application: application to protocol
        :param template: base document template
        :return: response document
        """
        document = copy.deepcopy(template)

        document["document_object"] = self.get_document_name(application, "outcome")
        document["document"] = self.get_attachment_data(application["outcome_file"])
        document["type"] = OUTCOME_TYPE
        document["arrival_date"] = application["latest_status_change_at"]
        document["source_type"] = APPLICATION_TYPE
        document["source_url"] = "{}/applications/{}".format(self.api_url, application["id"])
        document["meta"] = self.get_metadata(application, "outcome")
        document["signature"] = application["outcome_protocol_number"]

        for attachment in application["outcome_attachments"]:
            document["attachments"].append(self.get_attachment_data(attachment))
        return document

    def get_message_document(self, application, message, template, status_message=False):
        """
        Construction of a "message" type document
        :param application: application to protocol
        :param message: application message
        :param template: base document template
        :param status_message: Is message generated by a status change?
        :return: message document
        """
        document = copy.deepcopy(template)
        message["service"] = self.get_service_name(application)

        isIncome = application["user"] == message["author"]
        message_type = "user_message" if isIncome else "operator_message"
        # Message datetime placeholder substitution
        application["message_created_at"] = message["created_at"]
        document["document_object"] = self.get_document_name(application, message_type) if (
                    not status_message or not message["subject"]) else message["subject"]
        application.pop("message_created_at", None)
        document["document"] = self.generate_message_main_document(message)
        document["type"] = INCOME_TYPE if isIncome else OUTCOME_TYPE
        document["arrival_date"] = message["created_at"]
        document["source_type"] = MESSAGE_TYPE
        document["source_url"] = "{}/applications/{}/messages/{}".format(self.api_url, application["id"], message["id"])
        document["meta"] = self.get_metadata(application, message_type)
        document["signature"] = message["protocol_number"]

        for attachment in message["attachments"]:
            document["attachments"].append(self.get_attachment_data(attachment))

        return document

    def get_documents_from_application(self, application):
        """
        Construction of all documents relating to a given application that must be sent to the protocol system
        :param application: application to protocol
        :return: list of application's documents to protocol
        """
        documents = []
        application = self.integrate_application(application)
        template = self.get_document_template(application)

        documents.append(self.get_main_document(application, template))
        # Application response
        if self.record_responses and application["outcome_file"]:
            documents.append(self.get_response_document(application, template))

        for message in application["messages"]:
            if message["visibility"] == "applicant":
                # Message
                if self.record_messages == "always" or (
                        self.record_messages == "on-demand" and message["protocol_required"]):
                    # Check if messages has been flagged for protocol
                    documents.append(self.get_message_document(application, message, template))
            else:
                # Note
                if self.record_notes == "always" or \
                        (self.record_notes == "on-demand" and message["protocol_required"]):
                    # Check if messages has been flagged for protocol
                    documents.append(self.get_message_document(application, message, template))

        for message in application["status_messages"]:
            documents.append(self.get_message_document(application, message, template, True))

        return documents

    def integrate_application(self, application):
        """
        Merge application data with parent application data in case of related applications
        :param application: application to protocol
        :return: Enhanced application  containing all parent application data not included
        """
        if "related_applications" in application["data"]:
            related_application = self.get_application(application["data"]["related_applications"])
            application["data"] = merge_dict(application["data"], related_application['data'])
        return application

    def get_document_template(self, application):
        """
        Construction of the base document template needed to construct all documents types (application, response, message)
        :param application: application to protocol
        :return: base document template
        """
        document = {
            "project_object": self.get_project_name(application),
            "correspondent": self.get_correspondent(application),
            "attachments": []
        }
        return document

    def generate_content_from_template(self, data):
        """
        Generation of the contents of the file containing the message text
        :param data: message data
        :return: file content
        """
        # Get index.html template
        template = open("message_template.html", "rb")
        template_content = template.read()
        template.close()

        date = datetime.datetime.strptime(data['date'], '%Y-%m-%dT%H:%M:%S%z')

        # Replace index.html placeholders
        index_content = template_content.replace(
            b'%date%',
            bytes(datetime.datetime.strftime(date, '%d-%m-%Y'), encoding='utf8')
        )
        index_content = index_content.replace(
            b'%time%',
            bytes(datetime.datetime.strftime(date, '%H:%M:%S'), encoding='utf8')
        )

        index_content = index_content.replace(b'%logo_url%', bytes(self.logo_url, encoding='utf8'))
        index_content = index_content.replace(b'%tenant%', bytes(self.tenant, encoding='utf8'))
        index_content = index_content.replace(b'%service%', bytes(data["service"], encoding='utf8'))
        index_content = index_content.replace(b'%external_key%', bytes(data['external_key'], encoding='utf8'))
        index_content = index_content.replace(b'%text%', bytes(data['text'], encoding='utf8'))

        # Build attachments list
        attachments = data['attachments']
        if len(attachments) == 0:
            index_content = index_content.replace(b'%attachments_class%', bytes("d-none", encoding='utf8'))
        else:
            index_content = index_content.replace(b'%attachments_class%', bytes("d-block", encoding='utf8'))
            attachments_html = ""
            for attachment in attachments:
                attachments_html += "<li>{} &dash; {}</li>".format(
                    attachment['name'].replace("Allegato senza descrizione", "Allegato"),
                    self.get_remote_md5_sum(attachment['url'])
                )
            index_content = index_content.replace(b'%attachments%', bytes(attachments_html, encoding='utf8'))

        # Create pdf using gotenberg
        pdf_content = self.safe_gotenberg_request(index_content)

        if self.save_pdfs:
            # Write file to filesystem
            file_name = "pdf/{}/{}/{}.pdf".format(data['author'] if data['author'] else self.tenant,
                                                  data['external_key'], data['id'])
            if not os.path.exists(os.path.dirname(file_name)):
                try:
                    os.makedirs(os.path.dirname(file_name))
                except OSError as exc:  # Guard against race condition
                    if exc.errno != errno.EEXIST:
                        raise SDCException("SDC: An error occurred while creating directory {}".format(file_name))
            with open(file_name, "wb") as _file:
                _file.write(pdf_content)

        return pdf_content

    def generate_message_main_document(self, message):
        """
        Generate data required for the generation opf the file containing message data
        :param message: message
        :return: computed message data
        """

        data = {
            "date": message['created_at'],
            "service": message["service"],
            "external_key": message['application'],
            "attachments": message['attachments'],
            "author": message["author"],
            "id": message["id"],
            "text": message["message"],
        }

        return {
            "name": "Messaggio {}".format(message["id"]),
            "message_data": data,
            "original_name": "{}.pdf".format(message["id"])
        }

    def generate_authentication_attachment(self, application):
        """
        Generate data required for the generation of the xml containing authentication data
        :param application: application
        :return: computed authentication data
        """

        data = self.get_authentication_data(application)

        return {
            "name": "Segnatura",
            "authentication_data": data,
            "original_name": "Segnatura.xml"
        }

    def update_source(self, document):
        """
        Update of the application on the source system with the protocol data
        :param document: Protocolled document
        :return: None
        """
        protocol_datetime = datetime.datetime.strptime(document["protocol_datetime"], "%d/%m/%Y %H:%M:%S")
        protocol_datetime = protocol_datetime.strftime("%Y-%m-%dT%H:%M:%S%z")
        if document["source_type"] == MESSAGE_TYPE:
            payload = {
                "protocol_number": document["signature"],
                "protocolled_at": protocol_datetime
            }
        elif document["type"] == INCOME_TYPE:
            payload = {
                "protocol_folder_number": document["project_id"],
                "protocol_folder_code": document["project_code"],
                "protocol_number": document["signature"],
                "protocol_document_id": document["document_id"],
                "protocolled_at": protocol_datetime
            }
        else:
            payload = {
                "protocol_folder_number": document["project_id"],
                "protocol_folder_code": document["project_code"],
                "outcome_protocol_number": document["signature"],
                "outcome_protocol_document_id": document["document_id"],
                "outcome_protocolled_at": protocol_datetime
            }

        self.safe_update(
            url=document["source_url"],
            data=payload
        )

    def generate_xml(self, authentication_data):
        service_provider_value = authentication_data["service_provider"]
        root = gfg.Element("istanzeonline")

        service_provider = gfg.Element("serviceprovider", {"value": service_provider_value})
        root.append(service_provider)

        if service_provider_value == 'CPS/CNS':
            ca = gfg.SubElement(service_provider, "certificationauthority")
            if "certification_authority" in authentication_data and authentication_data["certification_authority"]:
                ca.text = authentication_data["certification_authority"]

        fiscal_code = gfg.SubElement(service_provider, "codicefiscale")
        if "fiscal_code" in authentication_data and authentication_data["fiscal_code"]:
            fiscal_code.text = authentication_data["fiscal_code"]

        surname = gfg.SubElement(service_provider, "cognome")
        if "surname" in authentication_data and authentication_data["surname"]:
            surname.text = authentication_data["surname"]

        name = gfg.SubElement(service_provider, "nome")
        if "name" in authentication_data and authentication_data["name"]:
            name.text = authentication_data["name"]

        authentication_date = gfg.SubElement(service_provider, "dataautenticazione")
        if "authentication_date" in authentication_data and authentication_data["authentication_date"]:
            authentication_date.text = authentication_data["authentication_date"]

        if service_provider_value == 'CPS/CNS':
            certificate_release_date = gfg.SubElement(service_provider, "datarilasciocertificato")
            if "certificate_release_date" in authentication_data and authentication_data["certificate_release_date"]:
                certificate_release_date.text = authentication_data["certificate_release_date"]

            certificate_due_date = gfg.SubElement(service_provider, "datascadenzacertificato")
            if "certificate_due_date" in authentication_data and authentication_data["certificate_due_date"]:
                certificate_due_date.text = authentication_data["certificate_due_date"]

            certificate_hash = gfg.SubElement(service_provider, "hashcertificato")
            if "certificate_hash" in authentication_data and authentication_data["certificate_hash"]:
                certificate_hash.text = authentication_data["certificate_hash"]

        module_id = gfg.SubElement(service_provider, "moduloId")
        if "module_id" in authentication_data and authentication_data["module_id"]:
            module_id.text = authentication_data["module_id"]

        if service_provider_value == 'CPS/CNS':
            certificate_number = gfg.SubElement(service_provider, "numerocertificato")
            if "certificate_number" in authentication_data and authentication_data["certificate_number"]:
                certificate_number.text = authentication_data["certificate_number"]

        authentication_time = gfg.SubElement(service_provider, "oraautenticazione")
        if "authentication_time" in authentication_data and authentication_data["authentication_time"]:
            authentication_time.text = authentication_data["authentication_time"]

        if service_provider_value == 'SPID':
            if "spid_code" in authentication_data and authentication_data["spid_code"]:
                spid_code = gfg.SubElement(service_provider, "spidcode")
                spid_code.text = authentication_data["spid_code"]

        authentication_method = gfg.SubElement(service_provider, "autenticationmethod")
        if "authentication_method" in authentication_data and authentication_data["authentication_method"]:
            authentication_method.text = authentication_data["authentication_method"]

        security = gfg.SubElement(service_provider, "sicurezza")
        if "security" in authentication_data and authentication_data["security"]:
            security.text = authentication_data["security"]

        hash_document = gfg.SubElement(service_provider, "hashdocumento")
        if "hash_document" in authentication_data and authentication_data["hash_document"]:
            hash_document.text = authentication_data["hash_document"]

        dom = xml.dom.minidom.parseString(gfg.tostring(root, encoding='utf-8'))
        pretty_xml_as_string = dom.toprettyxml(encoding="utf-8")

        if self.save_xmls:
            # Write file to filesystem
            file_name = "xmls/{}_{}_{}.xml".format(
                service_provider_value.replace("/", "-"),
                authentication_data["security"],
                authentication_data["application_id"]
            )
            if not os.path.exists(os.path.dirname(file_name)):
                try:
                    os.makedirs(os.path.dirname(file_name))
                except OSError as exc:  # Guard against race condition
                    if exc.errno != errno.EEXIST:
                        raise SDCException("SDC: An error occurred while creating directory {}".format(file_name))
            with open(file_name, "wb") as _file:
                _file.write(pretty_xml_as_string)
        return pretty_xml_as_string

    def get_remote_md5_sum(self, url):
        """
        Function to get the md5 of a remote file
        :param url: remote file url
        :param token: bearer token used for authentication
        :return:
        """
        response = self.safe_request(url=url, stream=True)

        sig = hashlib.md5()
        for line in response.iter_lines():
            sig.update(line)

        return sig.hexdigest()

    def get_remote_hash(self, url):
        """
        Function to get the hash of a remote file
        :param url: remote file url
        :param token: bearer token used for authentication
        :return:
        """

        response = self.safe_request(url=url, stream=True)

        sig = hashlib.sha256()
        for line in response.iter_lines():
            sig.update(line)

        return sig.hexdigest()
